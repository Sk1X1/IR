package search.vs;

import crawler.vs.Crawler;
import cz.zcu.kiv.nlp.ir.trec.data.Document;
import cz.zcu.kiv.nlp.ir.trec.data.IResult;
import indexer.utils.Posting;
import org.apache.log4j.Logger;
import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.cz.CzechAnalyzer;
import org.apache.lucene.queryparser.classic.ParseException;
import org.apache.lucene.queryparser.classic.QueryParser;
import org.apache.lucene.search.BooleanClause;
import org.apache.lucene.search.BooleanQuery;
import org.apache.lucene.search.Query;
import org.apache.lucene.search.TermQuery;
import preprocess.vs.Preprocessing;
import search.ir.Searcher;
import search.utils.QueryTerm;
import search.utils.Result;

import javax.print.Doc;
import java.util.*;
import java.util.stream.Collectors;

import static utils.Consts.*;

/**************************************************************
 * Search class.
 *
 * @version 1.0
 * @author Jiří Plaček
 **************************************************************/
public class Search implements Searcher {

    /** Logger of Crawler class */
    private static final Logger log = Logger.getLogger(Crawler.class);

    /** Inverted index */
    private Map<String, Map<String, Posting>> invertedIndex;

    /** List of query words */
    private List<QueryTerm> queryTermsList;

    /** List of must words */
    private List<QueryTerm> andList;

    /** List of banned documents */
    private List<String> banDocuments;

    /** List of possible documents */
    private Map<String, Integer> possibleDocuments;

    /** Document map from index */
    private Map<String, Document> documentMap;

    /**************************************************************
     * Default constructor.
     *
     * @param invertedIndex Inverted index to search in
     * @param documentMap Map of documents
     **************************************************************/
    public Search(Map<String, Map<String, Posting>> invertedIndex, Map<String, Document> documentMap) {
        this.invertedIndex = invertedIndex;
        this.documentMap = documentMap;

        this.queryTermsList = new ArrayList<>();
        this.andList = new ArrayList<>();
        this.possibleDocuments = new HashMap<>();
        this.banDocuments = new ArrayList<>();
    }

    @Override
    public List<IResult> search(String inputQuery) {
        return  search(inputQuery, MAX_RESULTS);
    }

    /**
     * Search with lucene.
     * If top < 0, then return all results, else only top
     *
     * @param inputQuery Query to search
     * @param top How many top results return
     * @return List of results
     */
    @Override
    public List<IResult> search(String inputQuery, int top) {
        log.info("Starting search for: " + inputQuery);

        //set up preprocess
        Preprocessing preprocessing = new Preprocessing(false);
        inputQuery = preprocessing.removeAccents(inputQuery);

        //set up parser
        Analyzer analyzer = new CzechAnalyzer();
        QueryParser parser = new QueryParser(FIELD_TERM, analyzer);
        Query parsedQuery = null;

        //parse query
        try {
            parsedQuery = parser.parse(inputQuery);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        int querySize = 0;
        //boolean or single term query?
        if(parsedQuery instanceof BooleanQuery) {
            BooleanQuery booleanQuery = (BooleanQuery) parsedQuery;
            for (BooleanClause clause : booleanQuery) {
                querySize++;

                // List of queries that should occur in resultList
                if (clause.getOccur() == BooleanClause.Occur.SHOULD) {
                    addIntoQueryTermList(clause.toString().replace("fterm:", ""));
                }
                // List of document's ID that have to occur in resultList
                else if (clause.getOccur() == BooleanClause.Occur.MUST) {
                    addIntoANDList(clause.toString().replace("+fterm:", ""));
                    addIntoQueryTermList(clause.toString().replace("+fterm:", ""));
                }
                // List of document's ID that must not occur in resultList
                else if (clause.getOccur() == BooleanClause.Occur.MUST_NOT) {
                    addIntoForbiddenDocList(clause.toString().replace("-fterm:", ""));
                }
            }
        } else {
            //not a boolean but only one term
            TermQuery termQuery = (TermQuery) parsedQuery;
            querySize = 1;

            addIntoANDList(termQuery.getTerm().text());
            addIntoQueryTermList(termQuery.getTerm().text());
        }

        //Set up query TF
        double sum = 0.0;
        for(QueryTerm q : queryTermsList) {
            double postingSize = invertedIndex.get(q.getTerm()).size();
            double idf = Math.log10(documentMap.size() / postingSize);
            q.setTf_idf((1 + Math.log10(q.getTermFrequency())) * idf);
            //q.setTf_idf(q.getTermFrequency() / querySize);
            //sum += q.getTf_idf() * q.getTf_idf();
        }

        //normalize
        /*sum = Math.sqrt(sum);
        for(QueryTerm q : queryTermsList) {
            q.setTf_idf(q.getTf_idf() / sum);
        }*/

        /*int sum = 0;
        for (double item : vector) {
            sum += item*item;
        }
        sum = Math.sqrt(sum);*/

        int i = 0;
        if(!andList.isEmpty()) {
            //sort list by posting list size
            andList.sort(Comparator.comparing(QueryTerm::getPostingListSize));

            //if and list is not empty, then find document by it
            for (QueryTerm queryTerm : andList) {
                //add all documents from first term
                if (i == 0) {
                    for (String docId : invertedIndex.get(queryTerm.getTerm()).keySet()) {
                        possibleDocuments.put(docId, 1);
                    }
                    i++;
                    continue;
                }
                List<String> removeList = new ArrayList<>();

                Map<String, Posting> doc1 = invertedIndex.get(queryTerm.getTerm());
                //go through document ids and
                for (String docId : possibleDocuments.keySet()) {
                    //check if document is in second posting list
                    if (doc1.get(docId) == null) {
                        //if it is not, add it into remove list
                        removeList.add(docId);
                    }
                }

                //remove documents form remove list
                for (String docId : removeList) {
                    possibleDocuments.remove(docId);
                }
            }
        }
        else {
            //andList is empty, go for queryList
            for (QueryTerm queryTerm : queryTermsList) {
                //take every document and add it into possible documents
                Set<Map.Entry<String, Posting>> postings = invertedIndex.get(queryTerm.getTerm()).entrySet();
                //for(Map.Entry<String, Posting> postingMap : invertedIndex.get(queryTerm.getTerm()).entrySet()) {
                for(Map.Entry<String, Posting> postingMap : postings) {
                    //if documents is already inside, then increase term frequency
                    Integer p;
                    if ((p = possibleDocuments.get(postingMap.getKey())) != null) {
                        //possibleDocuments.replace(postingMap.getKey(), p + postingMap.getValue().getTermFrequency());
                        possibleDocuments.replace(postingMap.getKey(), p + 1);
                    }
                    else{
                        //add it
                        //possibleDocuments.put(postingMap.getKey(), postingMap.getValue().getTermFrequency());
                        possibleDocuments.put(postingMap.getKey(), 1);
                    }
                }
            }
        }

        //remove banned documents
        for(String documentId : banDocuments) {
            possibleDocuments.remove(documentId);
        }

        /////////////////////////////////////////////////////////////////////
        //spoctu normu
        /*double norm = 0.0;
        for(String documentId : possibleDocuments.keySet()) {
            norm = 0.0;
            for (QueryTerm q : queryTermsList) {
                Map<String, Posting> postingList = invertedIndex.get(q.getTerm());
                if(postingList != null){
                    //set result score for document
                    Posting p = postingList.get(documentId);
                    //check if term is in document
                    if(p != null) {
                        norm += (p.getTf_idf() * p.getTf_idf());
                    }
                }
            }
        }

        norm = Math.sqrt(norm);*/

        //calculate norms
        /*Map<String, Double> norms = new HashMap<>();
        for(String documentId : possibleDocuments.keySet()) {
            for(Map<String, Posting> postingMap : invertedIndex.values()) {
                Posting p = postingMap.get(documentId);

                if(p != null) {
                    Double norm = norms.get(documentId);
                    if (norm == null) {
                        norm = Math.pow(p.getTf_idf(), 2);
                        norms.put(documentId, norm);
                    }
                    else {
                        norm += Math.pow(p.getTf_idf(), 2);
                        norms.replace(documentId, norm);
                    }
                }
            }
        }*/

        //calculate cosine similarity
        //loop through possible documents
        List<IResult> results = new ArrayList<>();
        for(String documentId : possibleDocuments.keySet()) {
            double resultScore = 0.0;
            double scoreQ = 0.0;
            double scoreP = 0.0;
            double norm = 0.0;

            //loop through query terms
            for(QueryTerm q : queryTermsList) {

                //get posting for term in document
                Map<String, Posting> postingList = invertedIndex.get(q.getTerm());
                if(postingList != null){
                    //set result score for document
                    Posting p = postingList.get(documentId);
                    //check if term is in document
                    if(p != null) {
                        resultScore += q.getTf_idf() * p.getTf_idf();

                        //resultScore += (q.getTf_idf() * (p.getTf_idf() / norm));
                        scoreQ += q.getTf_idf() * q.getTf_idf();
                        scoreP += p.getTf_idf() * p.getTf_idf();
                        /*norm += p.getTf_idf() * p.getTf_idf();*/
                    }
                }
            }

            //norm = Math.sqrt(norm);
            //double cosineSimilarity = scoreQ * (scoreP / norm);//resultScore;
            //double cosineSimilarity = resultScore / Math.sqrt(norms.get(documentId));
            scoreQ = Math.sqrt(scoreQ);
            scoreP = Math.sqrt(scoreP);
            double cosineSimilarity = resultScore / (scoreP * scoreQ);

            //add result into list
            if(cosineSimilarity == 1) {
                int r = 0;
            }
            results.add(new Result(documentId, cosineSimilarity /*+ possibleDocuments.get(documentId)*/, possibleDocuments.get(documentId)));
        }

        //order results and show them
        //results.sort(Comparator.comparing(IResult::getScore).reversed());

        int rank = 0;
        for (IResult r : results) {
            r.setRank(rank++);
        }

        //return only TOP X
        if(top < 0) {
            return results;
        }
        else {
            return results.subList(0, top > results.size() ? results.size() : top);
        }
    }

    /**
     * Search with custom index. Without boolean queries.
     * @param query Query
     * @return Results list
     */
    @Override
    public List<IResult> customSearch(String query) {
        log.info("Starting search for: " + query);

        Preprocessing preprocessing = new Preprocessing(false);
        Query luceneQuery;


        //TODO boolean query :(
        //check if query contains key words (AND, OR, NOT)
        boolean isBooleanQuery = false;
        if(query.contains(OPERATOR_AND)
                || query.contains(OPERATOR_OR)
                || query.contains(OPERATOR_NOT)) {
            isBooleanQuery = true;
        }


        //get tokens from query
        Map<String, Integer> tokens = preprocessing.preprocessQuery(query);

        //calculate size of query
        int querySize = 0;
        for(int i : tokens.values()){
            querySize += i;
        }

        //add tokens into list of qeury terms and AND-list
        for (Map.Entry<String, Integer> term : tokens.entrySet()) {
            addIntoANDList(new QueryTerm(term.getKey(), term.getValue()), term.getValue());
            addIntoQueryTermList(new QueryTerm(term.getKey(), term.getValue()), term.getValue());
        }

        //find all documents for query
        for(QueryTerm term : andList) {
            Map<String, Posting> indexEntry;
            if((indexEntry = invertedIndex.get(term.getTerm())) != null) {
                term.setPostingListSize(indexEntry.size());
            }
            else
            {
                log.error("No document with this term:" + term);
                //TODO vypsat chybu
            }
        }

        //sort list by posting list size
        Collections.sort(andList, Comparator.comparing(QueryTerm::getPostingListSize));

        //resolve possible documents for every must term
        int i = 0;
        for(QueryTerm queryTerm: andList) {
            //add all documents from first term
            if(i == 0) {
                for(String docId : invertedIndex.get(queryTerm.getTerm()).keySet()){
                    possibleDocuments.put(docId, 0);
                }
                i++;
                continue;
            }
            List<String> removeList = new ArrayList<>();

            Map<String, Posting> doc1 = invertedIndex.get(queryTerm.getTerm());
            //go through document ids and
            for(String docId : possibleDocuments.keySet()){
                //check if document is in second posting list
                if(doc1.get(docId) == null) {
                    //if it is not, add it into remove list
                    removeList.add(docId);
                }
            }

            //remove documents form remove list
            for(String docId : removeList) {
                possibleDocuments.remove(docId);
            }
        }

        //todo NOT documents

        //calculate cosine similarity
        //loop through possible documents
        List<IResult> results = new ArrayList<>();
        for(String documentId : possibleDocuments.keySet()) {
            double resultScore = 0.0;
            //loop through query terms
            for(QueryTerm q : queryTermsList) {

                //get posting for term in document
                Map<String, Posting> postingList = invertedIndex.get(q.getTerm());
                if(postingList != null){
                    //set result score for document
                    Posting p = postingList.get(documentId);
                    resultScore = resultScore + (q.getTf_idf() * p.getTf_idf());
                }
            }

            //calculate normalized cosine similarity
            int documentLength = documentMap.get(documentId).getText().length();
            double cosineSimilarity = resultScore / Math.sqrt((querySize * querySize) * (documentLength * documentLength));

            //add result into list
            results.add(new Result(documentId, cosineSimilarity, possibleDocuments.get(documentId)));
        }

        //order results and show them
        results.sort(Comparator.comparing(IResult::getScore).reversed());

        //return only TOP X
        results.subList(0, MAX_RESULTS > results.size() ? results.size() : MAX_RESULTS);

        return results;
    }

    /**
     * Add token into list of words that are in query.
     *
     * @param term Term to add
     * @param count Term frequency in query
     */
    private void addIntoQueryTermList(QueryTerm term, int count) {
        if(!queryTermsList.contains(term.getTerm())) {
            //get number of documents wity term
            int postingSize = invertedIndex.get(term.getTerm()).size();
            //calculate idf
            double idf = Math.log10(documentMap.size() / postingSize);
            term.setTf_idf((1 + Math.log10(count)) * idf);
            queryTermsList.add(term);
        }
    }

    /**
     * Add token into list of words that has to be in document.
     *
     * @param term Term to add
     * @param count Term frequency in query
     */
    private void addIntoANDList(QueryTerm term, int count) {
        if(!andList.contains(term.getTerm())) {
            andList.add(term);
        }
    }

    /**
     * Add token into list of words that are in query.
     *
     * @param term Term to add
     */
    private void addIntoQueryTermList(String term) {
        QueryTerm queryTerm = new QueryTerm(term);
        int index = queryTermsList.indexOf(queryTerm);

        if(index < 0) {
            //get number of documents term
            if(invertedIndex.get(term) != null) {
                //int postingSize = invertedIndex.get(term).size();
                //calculate idf
                //double idf = Math.log10(documentMap.size() / postingSize);

                //QueryTerm qt = new QueryTerm(term);
                //qt.setTf_idf((1 + Math.log(count)) * idf);
                //queryTerm.setTermFrequency(queryTerm.getTermFrequency() + 1);
                queryTerm.setTermFrequency(1);
                queryTermsList.add(queryTerm);
            }
        }
        else {
            queryTerm = queryTermsList.get(index);
            queryTerm.setTermFrequency(queryTerm.getTermFrequency() + 1);
        }
    }

    /**
     * Add token into list of words that has to be in document.
     *
     * @param term Term to add
     */
    private void addIntoANDList(String term) {
        if(!andList.contains(term)) {
            QueryTerm qt = new QueryTerm(term);
            //qt.setTf_idf((1 + Math.log(count)) * idf);
            qt.setTermFrequency(1);
            andList.add(qt);
        }
    }

    /**
     * Add token into list of docuemnts that cannot be in result.
     * @param term  current term
     */
    private void addIntoForbiddenDocList(String term) {
        for(String docId : invertedIndex.get(term).keySet()) {
            if(banDocuments.contains(term)){
                banDocuments.add(docId);
            }
        }
    }
}
