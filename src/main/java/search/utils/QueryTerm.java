package search.utils;

/**************************************************************
 * Class representing one query term.
 *
 * @version 1.0
 * @author Jiří Plaček
 **************************************************************/
public class QueryTerm {

    /** Query term */
    private String term;
    /** Posting list size */
    private int postingListSize;
    /** Td-idf */
    private double tf_idf;
    /** Term frequency */
    private double termFrequency;

    public QueryTerm(String term) {
        this.term = term;
    }

    public QueryTerm(String term, double tf_idf) {
        this.term = term;
    }


    public String getTerm() {
        return term;
    }

    public void setTerm(String term) {
        this.term = term;
    }

    public int getPostingListSize() {
        return postingListSize;
    }

    public void setPostingListSize(int postingListSize) {
        this.postingListSize = postingListSize;
    }

    public double getTf_idf() {
        return tf_idf;
    }

    public void setTf_idf(double tf_idf) {
        this.tf_idf = tf_idf;
    }


    public double getTermFrequency() {
        return termFrequency;
    }

    public void setTermFrequency(double termFrequency) {
        this.termFrequency = termFrequency;
    }

    @Override
    public boolean equals(Object v) {
        boolean retVal = false;

        if (v instanceof QueryTerm){
            QueryTerm ptr = (QueryTerm) v;
            retVal = ptr.getTerm().equals(this.term);
        }

        return retVal;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 17 * hash + (this.term != null ? this.term.hashCode() : 0);
        return hash;
    }
}
