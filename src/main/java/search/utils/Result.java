package search.utils;


import cz.zcu.kiv.nlp.ir.trec.data.IResult;

/**
 * Class representing one result of search.
 *
 * @version 1.0
 * @author Jiří Plaček
 */
public class Result implements IResult {
    /** Document id */
    private String documentId;
    /** Document score */
    private double score;
    /** Document rank */
    private int rank;

    /**
     * Default constructor.
     *
     * @param documentId Document id
     * @param score Result score
     */
    public Result(String documentId, double score, int rank) {
        this.documentId = documentId;
        this.score = score;
        this.rank = rank;
    }


    public String getDocumentId() {
        return documentId;
    }

    public void setDocumentId(String documentId) {
        this.documentId = documentId;
    }

    @Override
    public String getDocumentID() {
        return documentId;
    }

    public double getScore() {
        return score;
    }

    @Override
    public int getRank() {
        return rank;
    }

    @Override
    public void setRank(int rank) {
        this.rank = rank;
    }

    @Override
    public String toString(String topic) {
        return topic + " Q0 " + documentId+ " " + rank + " " + score + " runindex1";
    }

    public void setScore(double score) {
        this.score = score;
    }

    @Override
    public boolean equals(Object v) {
        boolean retVal = false;

        if (v instanceof Result){
            Result ptr = (Result) v;
            retVal = ptr.getScore() == this.score;
        }

        return retVal;
    }
}
