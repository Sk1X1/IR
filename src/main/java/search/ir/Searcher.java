package search.ir;

import cz.zcu.kiv.nlp.ir.trec.data.IResult;

import java.util.List;

public interface Searcher {
    List<IResult> search(String query);
    List<IResult> search(String query, int top);
    List<IResult> customSearch(String query);
}
