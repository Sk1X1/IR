package preprocess.ir;


import org.apache.log4j.Logger;
import utils.Record;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import static utils.Consts.*;

/**
 * Created by Tigi on 29.2.2016.
 */
public class BasicPreprocessing implements IPreprocessing {

    static final Logger log = Logger.getLogger(BasicPreprocessing.class);

    Map<String, Integer> wordFrequencies = new HashMap<String, Integer>();
    IStemmer IStemmer;
    ITokenizer tokenizer;
    Set<String> stopwords;
    boolean removeAccentsBeforeStemming;
    boolean removeAccentsAfterStemming;
    boolean toLowercase;

    public BasicPreprocessing(IStemmer IStemmer, ITokenizer tokenizer, Set<String> stopwords, boolean removeAccentsBeforeStemming, boolean removeAccentsAfterStemming, boolean toLowercase) {
        this.IStemmer = IStemmer;
        this.tokenizer = tokenizer;
        this.stopwords = stopwords;
        this.removeAccentsBeforeStemming = removeAccentsBeforeStemming;
        this.removeAccentsAfterStemming = removeAccentsAfterStemming;
        this.toLowercase = toLowercase;
    }

    /**************************************************************
     * Preprocess one record.
     *
     * @param document Processed record
     * @return Tokens from record
     **************************************************************/
    @Override
    public Map<String, Integer> index(Record document) {
        Map<String, Integer> tokens = new HashMap<>();
        String title = document.getTitle();
        String content = document.getContent();

        if (toLowercase) {
            title = title.toLowerCase();
            content = content.toLowerCase();
        }
        if (removeAccentsBeforeStemming) {
            title = removeAccents(title);
            content = removeAccents(content);
        }

        String finalStr = "";

        for (String token : tokenizer.tokenize(title)) {

            if (token.contains("<a href="))
            {
                token = token.replace("<a href=\"", "");
                token = token.replace("\">", "");
            }

            if(stopwords.contains(token)) {
                continue;
            }
            if (IStemmer != null) {
                token = IStemmer.stem(token);
            }
            if (removeAccentsAfterStemming) {
                token = removeAccents(token);
            }
            if (!tokens.containsKey(token)) {
                tokens.put(token, 0);
            }

            tokens.put(token, tokens.get(token) + 1);
        }

        for (String token : tokenizer.tokenize(content)) {

            if (token.contains("<a href="))
            {
                token = token.replace("<a href=\"", "");
                token = token.replace("\">", "");
            }

            if(stopwords.contains(token)) {
                continue;
            }
            if (IStemmer != null) {
                token = IStemmer.stem(token);
            }
            if (removeAccentsAfterStemming) {
                token = removeAccents(token);
            }
            if (!tokens.containsKey(token)) {
                tokens.put(token, 0);
            }

            tokens.put(token, tokens.get(token) + 1);
        }

        return tokens;
    }

    @Override
    public void index(String document, boolean intoFile) {
        if (toLowercase) {
            document = document.toLowerCase();
        }
        if (removeAccentsBeforeStemming) {
            document = removeAccents(document);
        }

        String finalStr = "";

        for (String token : tokenizer.tokenize(document)) {

            if (token.contains("<a href="))
            {
                token = token.replace("<a href=\"", "");
                token = token.replace("\">", "");
            }

            if(stopwords.contains(token)) {
                continue;
            }
                if (IStemmer != null) {
                    token = IStemmer.stem(token);
                }
                if (removeAccentsAfterStemming) {
                token = removeAccents(token);
            }
            if (!wordFrequencies.containsKey(token)) {
                wordFrequencies.put(token, 0);
            }

            wordFrequencies.put(token, wordFrequencies.get(token) + 1);
            finalStr = token;
        }
        if(intoFile) {
            try {
                PrintWriter out = new PrintWriter(new BufferedWriter(new FileWriter(STORAGE + PREPROCESSED_FILE_NAME, true)));
                out.println(finalStr);
                out.close();
            } catch (IOException e) {
                log.error("Chyba zápisu!!");
            }
        }
    }

    @Override
    public void index(String document) {
        this.index(document, false);
    }

    @Override
    public String getProcessedForm(String text) {
        if (toLowercase) {
            text = text.toLowerCase();
        }
        if (removeAccentsBeforeStemming) {
            text = removeAccents(text);
        }
        if (IStemmer != null) {
            text = IStemmer.stem(text);
        }
        if (removeAccentsAfterStemming) {
            text = removeAccents(text);
        }
        return text;
    }

    final String withDiacritics = "áÁćĆčČďĎéÉěĚíÍňŇóÓřŘšŠťŤúÚůŮýÝžŽ";
    final String withoutDiacritics = "aAcCcCdDeEeEiInNoOrRsStTuUuUyYzZ";

    public String removeAccents(String text) {
        for (int i = 0; i < withDiacritics.length(); i++) {
            text = text.replaceAll("" + withDiacritics.charAt(i), "" + withoutDiacritics.charAt(i));
        }
        return text;
    }

    public Map<String, Integer> getWordFrequencies() {
        return wordFrequencies;
    }
}
