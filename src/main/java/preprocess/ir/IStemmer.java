package preprocess.ir;

/**
 * Created by tigi on 29.2.2016.
 */
public interface IStemmer {
    String stem(String input);
}
