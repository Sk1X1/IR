/**
 * Copyright (c) 2014, Michal Konkol
 * All rights reserved.
 */
package preprocess.ir;

import java.text.Normalizer;
import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author Michal Konkol
 */
public class AdvancedTokenizer implements ITokenizer {
    public static final String defaultRegex = "\\b(https?|ftp|file)://[-a-zA-Z0-9+&@#/%?=~_|!:,.;]*[-a-zA-Z0-9+&@#/%=~_|]|(\\d+\\.\\d+\\.(\\d+)?)|(\\d+[.,](\\d+))|[\\p{Alpha}\\d]([\\p{Alpha}\\d]+[*]?)*[\\p{Alpha}\\d]";
    //public static final String defaultRegex = "\\b(https?|ftp|file)://[-a-zA-Z0-9+&@#/%?=~_|!:,.;]*[-a-zA-Z0-9+&@#/%=~_|]|(\\d+\\.\\d+\\.(\\d+)?)|(\\d+[.,](\\d+))|[\\p{L}\\d]([\\p{L}\\d]+[*]?)*[\\p{L}\\d]|(<.*?>)";
    public static String[] tokenize(String text, String regex) {
        Pattern pattern = Pattern.compile(regex);

        ArrayList<String> words = new ArrayList<String>();

        Matcher matcher = pattern.matcher(text);
        while (matcher.find()) {
            int start = matcher.start();
            int end = matcher.end();

            String test = text.substring(start, end);

            words.add(test);
        }

        String[] ws = new String[words.size()];
        ws = words.toArray(ws);

        return ws;
    }

    public static String removeAccents(String text) {
        return text == null ? null : Normalizer.normalize(text, Normalizer.Form.NFD).replaceAll("\\p{InCombiningDiacriticalMarks}+", "");
    }

    @Override
    public String[] tokenize(String text) {
        return tokenize(text, defaultRegex);
    }
}
