package preprocess.ir;

import utils.Record;

import java.util.Map;

/**
 * Created by tigi on 29.2.2016.
 */
public interface IPreprocessing {
    void index(String document, boolean intoFile);
    void index(String document);
    Map<String, Integer> index(Record document);
    String getProcessedForm(String text);

    Map<String, Integer> getWordFrequencies();
    String removeAccents(String text);
}
