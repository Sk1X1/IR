package preprocess.vs;

import cz.zcu.kiv.nlp.ir.trec.data.Document;
import org.apache.log4j.Logger;
import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.CharArraySet;
import org.apache.lucene.analysis.StopFilter;
import org.apache.lucene.analysis.TokenStream;
import org.apache.lucene.analysis.cz.CzechAnalyzer;
import org.apache.lucene.analysis.tokenattributes.CharTermAttribute;
import preprocess.ir.AdvancedTokenizer;
import preprocess.ir.BasicPreprocessing;
import preprocess.ir.CzechStemmerAgressive;
import preprocess.ir.IPreprocessing;
import utils.Record;

import java.io.*;
import java.util.*;

import static utils.Consts.*;

/**************************************************************
 * Preprocessing class.
 *
 * @version 1.0
 * @author Jiří Plaček
 **************************************************************/
public class Preprocessing {
    /** Logger of Preprocessing class */
    private static final Logger log = Logger.getLogger(Preprocessing.class);
    /** Data to process */
    private List<Document> data;
    /** Set of stop words */
    private Set<String> stopWords;
    /** Preprocess class */
    private IPreprocessing preprocessing;
    /** Analyzer */
    private Analyzer analyzer;

    /**************************************************************
     * Constructor, it will preprocess data from default
     * path (./storage/data.ser).
     **************************************************************/
    public Preprocessing() {
        this( STORAGE + DEFAULT_FILE_NAME + DEFAULT_FILE_EXTENSION);
    }

    /**************************************************************
     * Constructor, It will preprocess given data.
     *
     * @param pathToData Path to file with data.
     **************************************************************/
    public Preprocessing(String pathToData) {
        initPreprocessing();
        loadData(pathToData);
    }

    /**************************************************************
     * Preprocessing class. It can initialize without loading data.
     *
     * @param loadData Load data or not
     **************************************************************/
    public Preprocessing(boolean loadData) {
        initPreprocessing();
        if(loadData) {
            loadData(STORAGE + DEFAULT_FILE_NAME + DEFAULT_FILE_EXTENSION);
        }
    }

    /**************************************************************
     * Load data.
     **************************************************************/
    private void loadData(String pathToData) {
        log.info("Start loading data from: " + pathToData);
        System.out.println("Začínám načítat data ze souboru: " + pathToData);
        data = Document.load(pathToData);
        log.info("Data loaded from: " + pathToData);
        System.out.println("Data načtena.");
    }

    /**
     * Load stop words from file.
     * @param path Path to file
     * @return Set of stop words
     */
    private Set<String> loadStopWords(String path){
        Set<String> stopWords = new HashSet<>();

        try {
            File f = new File(path);
            BufferedReader b = new BufferedReader(new FileReader(f));
            String readLine = "";
            while ((readLine = b.readLine()) != null) {
                //one line = one world
                Record record = new Record();
                //set word as content of record
                record.setTitle("");
                record.setContent(readLine);
                //preprocess record
                List<String> stopWord = preprocessRecord(record, false);
                //add preprocess word into stop words
                if(!stopWord.isEmpty()) {
                    stopWords.add(stopWord.get(0));
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        return stopWords;
    }

    /**************************************************************
     * Get stop words as CharArraySet.
     **************************************************************/
    private CharArraySet getStopWords() {
        return new CharArraySet(stopWords, true);
    }

    /**************************************************************
     * Initialize preprocessing class.
     **************************************************************/
    private void initPreprocessing() {
        analyzer = new CzechAnalyzer();
        stopWords = loadStopWords(STOP_WORDS_FILE_NAME);
        preprocessing = new BasicPreprocessing(
            new CzechStemmerAgressive(), new AdvancedTokenizer(), stopWords, true, false, true
        );
    }

    /**************************************************************
     * Custom preprocess.
     *
     * @param record Record to preprocess
     * @return List of tokens from record
     **************************************************************/
    public Map<String, Integer> preprocessRecord2(Record record) {
        if(preprocessing == null) {
            initPreprocessing();
        }

        if(stopWords.isEmpty()) {
            //nacist stop slova a odstranit z nich diakritiku
            Set<String> stopWords = loadStopWords("stopwords.txt");
            for (String word : stopWords) {
                this.stopWords.add(preprocessing.getProcessedForm(word));
            }
        }
        return preprocessing.index(record);
    }

    /**
     * Preprocess for TestTrec with lucene.
     *
     * @param record Record to preprocess
     * @return List of tokens from record
     */
    /*public List<String> preprocessRecord2(Document record, boolean removeStopWords) {
        //Preprocessing preprocessing = new Preprocessing();
        List<String> tokens = new ArrayList<>();
        //create final text to preprocess
        String text = record.getTitle() + " " + record.getText();
        //remove accent
        text = removeAccents(text);

        //Analyzer analyzer = new CzechAnalyzer();
        try {
            TokenStream stream  = analyzer.tokenStream(null, new StringReader(text));
            if(removeStopWords) {
                stream = new StopFilter(stream, getStopWords());
            }
            stream.reset();

            while (stream.incrementToken()) {
                String token = stream.getAttribute(CharTermAttribute.class).toString();
                tokens.add(token);
            }
            stream.close();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }

        return tokens;
    }*/

    /**
     * Preprocess with lucene.
     *
     * @param record Record to preprocess
     * @param removeStopWords If stop words should be removed
     * @return List of tokens from record
     */
    public List<String> preprocessRecord(Document record, boolean removeStopWords) {
        List<String> tokens = new ArrayList<>();
        //create final text to preprocess
        String text = record.getTitle() + " " + record.getText();
        //remove accent
        text = removeAccents(text);

        try {
            TokenStream stream  = analyzer.tokenStream(null, new StringReader(text));
            if(removeStopWords) {
                stream = new StopFilter(stream, getStopWords());
            }
            stream.reset();

            while (stream.incrementToken()) {
                String token = stream.getAttribute(CharTermAttribute.class).toString();
                tokens.add(token);
            }
            stream.close();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }

        return tokens;
    }

    /**************************************************************
     * Preprocess given query
     *
     * @param query Query to preprocess
     * @return Tokens from query
     **************************************************************/
    public Map<String, Integer> preprocessQuery(String query) {

        Record rec = new Record();
        rec.setTitle("");
        rec.setContent(query);

        return preprocessRecord2(rec);
    }

    /**************************************************************
     * Preprocess loaded data.
     *
     * @param intoFile Save results in file or not
     **************************************************************/
    public void preprocessData(boolean intoFile) {
        System.out.println("Zahajuji preprocessing.");
        if(preprocessing == null) {
            initPreprocessing();
        }

        if(stopWords.isEmpty()) {
            //nacist stop slova a odstranit z nich diakritiku
            Set<String> stopWords = loadStopWords("stopwords.txt");
            for (String word : stopWords) {
                this.stopWords.add(preprocessing.getProcessedForm(word));
            }
        }

        //premazu soubor
        if(intoFile) {
            try {
                PrintWriter out2 = new PrintWriter(new BufferedWriter(new FileWriter(STORAGE + PREPROCESSED_FILE_NAME, false)));
                out2.close();
            } catch (IOException e) {
                log.error("Chyba zápisu!!");
            }
        }

        long start = System.nanoTime();
        int count = 0;

        for (Document r : data) {
            String post = r.getTitle() + " " + r.getText();
            preprocessing.index(post, intoFile);

            count++;

            if (count % 1000 == 0){
                log.info("Indexed: " + count);
                System.out.println("Zpracováno: " + count);
            }
        }
        long elapsedTime = System.nanoTime() - start;

        log.info("Totally indexed: " + count);
        log.info("Duration: " + (elapsedTime / 1000000000) + " sec");
        System.out.println("Zpracování dokončeno.");
        System.out.println("Zpracováno celkem: " + count);
        System.out.println("Data uložena do souboru: " + STORAGE + PREPROCESSED_FILE_NAME);
    }

    /**************************************************************
     * Return loaded data.
     *
     * @return List of records
     **************************************************************/
    public List<Document> getData() {
        return this.data;
    }

    public String getProcessedForm(String text) {
        return preprocessing.getProcessedForm(text);
    }

    final String withDiacritics = "áÁćĆčČďĎéÉěĚíÍňŇóÓřŘšŠťŤúÚůŮýÝžŽ";
    final String withoutDiacritics = "aAcCcCdDeEeEiInNoOrRsStTuUuUyYzZ";

    public String removeAccents(String text) {
        for (int i = 0; i < withDiacritics.length(); i++) {
            text = text.replaceAll("" + withDiacritics.charAt(i), "" + withoutDiacritics.charAt(i));
        }
        return text;
    }
}
