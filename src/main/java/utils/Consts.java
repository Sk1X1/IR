package utils;

/**************************************************************
 * Class containing constants of searcher.
 *
 * @version 1.0
 * @author Jiří Plaček
 **************************************************************/
public final class Consts {

    // Program parameters
    /** Download data */
    public static final String DOWNLOAD_PAR = "-d";
    /** Preprocess data */
    public static final String PREPROCESS_PAR = "-p";
    /** Index data */
    public static final String INDEX_PAR = "-i";
    /** Search */
    public static final String SEARCH_PAR = "-s";
    /** Eval */
    public static final String EVAL_PAR = "-e";

    /** Path to driver */
    //public static final String DRIVER_PATH = "/home/sk1x1/Stažené/chromedriver_linux64/chromedriver";
    public static final String DRIVER_PATH = "chromedriver.exe";

    /** Path to save downloaded data */
    public static final String STORAGE = "./storage/";
    /** Default file extension */
    public static final String DEFAULT_FILE_EXTENSION = ".ser";
    /** Default file name */
    public static final String DEFAULT_FILE_NAME = "data";
    /** Index file name*/
    public static final String INDEX_FILE_NAME = "index";
    /** Document map file name*/
    public static final String DOCUMENT_MAP_FILE_NAME = "documentMap";
    /** Path to save downloaded data (topics) */
    public static final String BOARD_TOPICS_FOLDER_PATH = "topics/";
    /** Website to download */
    public static final String SITE = "http://30kmh.cz";

    /** Paging in boards */
    public static int BOARD_PAGING = 20;
    /** Paging in topics*/
    public static int TOPIC_PAGING = 15;


    //**************************** utils.Record xPath  section ***************************/
    /** Title key */
    public static final String TITLE_KEY = "Title";
    /** Content key */
    public static final String CONTENT_KEY = "Content";
    /** User key */
    public static final String USER_KEY = "User";
    /** Post title */
    public static final String TITLE_XPATH = "//ul[@class='linktree']/li[@class='last']";
    /** Post content */
    public static final String CONTENT_XPATH = "//div[@class='post']/div[@class='inner']";
    /** User who created post*/
    public static final String USER_XPATH = "//div[@class='bordercolor']//h4";
    /** Number of posts created by user*/
    //public static final String numberOfUserPost = "";
    /** User who created post*/
    public static final String DATE_XPATH = "";

    /** Stopwords file name */
    public static final String STOP_WORDS_FILE_NAME = "stopwords.txt";
    /** Preprocessed data file name */
    public static final String PREPROCESSED_FILE_NAME = "preprocessedData";

    /** Field term */
    public static final String FIELD_TERM = "fterm";

    /** AND operator */
    public static final String OPERATOR_AND = "AND";
    /** OR operator */
    public static final String OPERATOR_OR = "OR";
    /** NOT operator */
    public static final String OPERATOR_NOT = "NOT";

    /** MAX results */
    public static final int MAX_RESULTS = 1000;
}
