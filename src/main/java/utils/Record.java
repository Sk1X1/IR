package utils;

import cz.zcu.kiv.nlp.ir.trec.data.Document;

import java.io.Serializable;
import java.sql.Date;

/**************************************************************
 * Class representing one record (post) of downloaded data.
 *
 * @version 1.0
 * @author Jiří Plaček
 **************************************************************/
public class Record implements Serializable, Document {

    /** Version ID */
    private static final long serialVersionUID = 1L;

    /**Post id */
    private String id;

    /** Post section */
    private String section;
    /** Post title */
    private String title;
    /** Post content */
    private String content;
    /** User who created post*/
    private String user;

    /** Number of posts created by user*/
    private String numberOfUserPost;
    /** Date of post creation*/
    private Date date;

    /**************************************************************
     * Default constructor.
     **************************************************************/
    public Record() {

    }

    /**************************************************************
     * Default constructor.
     *
     * @param id Id
     * @param title Title
     * @param content Content
     * @param user User
     **************************************************************/
    public Record(String id, String title, String content, String user) {
        this.id = id;
        this.title = title;
        this.content = content;
        this.user = user;
    }

    /**************************************************************
     * Return post section.
     *
     * @return Post section
     **************************************************************/
    public String getSection() {
        return section;
    }

    /**************************************************************
     * Set post section.
     *
     * @param section Post section
     **************************************************************/
    public void setSection(String section) {
        this.section = section;
    }

    /**************************************************************
     * Return post title.
     *
     * @return Post title
     **************************************************************/
    public String getTitle() {
        return title;
    }

    /**************************************************************
     * Set post title.
     *
     * @param title Post title
     **************************************************************/
    public void setTitle(String title) {
        this.title = title;
    }

    /**************************************************************
     * Return post content.
     *
     * @return Post content
     **************************************************************/
    public String getContent() {
        return content;
    }

    /**************************************************************
     * Set post content.
     *
     * @param content Post content
     **************************************************************/
    public void setContent(String content) {
        this.content = content;
    }

    /**************************************************************
     * Return post user.
     *
     * @return Post user
     **************************************************************/
    public String getUser() {
        return user;
    }

    /**************************************************************
     * Set post user.
     *
     * @param user Post user
     **************************************************************/
    public void setUser(String user) {
        this.user = user;
    }

    /**************************************************************
     * Return number of posts created by user.
     *
     * @return Number of posts created by user
     **************************************************************/
    public String getNumberOfUserPost() {
        return numberOfUserPost;
    }

    /**************************************************************
     * Set number of user's post.
     *
     * @param numberOfUserPost Set number of users post
     **************************************************************/
    public void setNumberOfUserPost(String numberOfUserPost) {
        this.numberOfUserPost = numberOfUserPost;
    }

    /**************************************************************
     * Return post creation date.
     *
     * @return Post creation date
     **************************************************************/
    public Date getDate() {
        return date;
    }

    /**************************************************************
     * Set post creation date.
     *
     * @param date Post creation date
     **************************************************************/
    public void setDate(Date date) {
        this.date = date;
    }

    @Override
    public String getText() {
        return getContent();
    }

    /**************************************************************
     * Return post id.
     *
     * @return Post id
     **************************************************************/
    public String getId() {
        return id;
    }

    /**************************************************************
     * Set post id.
     *
     * @param id Post id
     **************************************************************/
    public void setId(String id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return "utils.Record{" +
                "section='" + section + '\'' +
                ", title='" + title + '\'' +
                ", content='" + content + '\'' +
                ", user='" + user + '\'' +
                ", numberOfUserPost='" + numberOfUserPost + '\'' +
                ", date=" + date +
                '}';
    }
}
