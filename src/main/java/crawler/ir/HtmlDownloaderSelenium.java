package crawler.ir;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import us.codecraft.xsoup.Xsoup;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static utils.Consts.DRIVER_PATH;


/**************************************************************
 * Class downloading a website.
 *
 * @version 1.0
 * @author Jiří Plaček
 **************************************************************/
public class HtmlDownloaderSelenium extends AbstractHtmlDownloader {

    /** Selenium driver */
    private WebDriver driver;

    /**************************************************************
     * Defauld constructor. Set up driver.
     **************************************************************/
    public HtmlDownloaderSelenium() {
        super();
        System.setProperty("webdriver.chrome.driver", DRIVER_PATH);
        driver = new ChromeDriver();
    }

    /**************************************************************
     * Download given url page and extracts xpath expression.
     *
     * @param url      Page url
     * @param xpathMap Pairs of description and xpath expression
     * @return Pairs of descriptions and extracted values
     **************************************************************/
    public Map<String, List<String>> processUrl(String url, Map<String, String> xpathMap) {
        Map<String, List<String>> results = new HashMap<String, List<String>>();

        log.info("Processing: " + url);
        driver.get(url);
        String dom = driver.getPageSource();
        if (dom != null) {
            Document document = Jsoup.parse(dom);

            for (String key : xpathMap.keySet()) {
                ArrayList<String> list = new ArrayList<>(Xsoup.compile(xpathMap.get(key)).evaluate(document).list());
                results.put(key, list);
            }
        } else {
            log.info("Couldn't fetch the content of the page.");
            failedLinks.add(url);
        }
        return results;
    }


    /**************************************************************
     * Download given url page and extracts xpath expression.
     *
     * @param url      Page url
     * @param xPath    Xpath expression
     * @return List of extracted values
     **************************************************************/
    public List<String> getLinks(String url, String xPath) {
        ArrayList<String> list = new ArrayList<>();

        log.info("Processing: " + url);
        driver.get(url);
        String dom = driver.getPageSource();
        if (dom != null) {
            Document document = Jsoup.parse(dom);
            List<String> xlist = Xsoup.compile(xPath).evaluate(document).list();
            list.addAll(xlist);
        } else {
            log.info("Couldn't fetch the content of the page.");
            failedLinks.add(url);
        }
        return list;
    }

    /**************************************************************
     * Finds maximal page number and return it.
     *
     * @param url           Page url
     * @param cssMaxPage    Css path to element
     * @return Maximal page
     **************************************************************/
    public int getMaximalPage(String url, String cssMaxPage) {
        log.info("Finding max page on: " + url);
        int maxPage = -1;
        driver.get(url);
        String dom = driver.getPageSource();
        if (dom != null) {
            Document document = Jsoup.parse(dom);
            //List<String> xlist = Xsoup.compile(xPath).evaluate(document).list();
            /*try {
                maxPage = Integer.parseInt(Xsoup.compile(xPath).evaluate(document).get());
            }
            catch (NumberFormatException e) {
                maxPage = -1;
            }*/
            Element element = document.select(cssMaxPage).last();
            //if element is null, then there is only one page
            if (element == null) {
                maxPage = 1;
            }
            else {
                String test = element.text();
                try {
                    maxPage = Integer.parseInt(test);
                }
                catch (NumberFormatException e) {
                    maxPage = -1;
                }
            }

            //#modbuttons_bottom > div > a:nth-child(5)
            //<a class="navPages" href="https://www.30kmh.cz/index.php?board=32.280">15</a>
        } else {
            log.info("Couldn't fetch the content of the page.");
            failedLinks.add(url);
        }
        return maxPage;
    }

    /**************************************************************
     * Quit driver (browser).
     **************************************************************/
    public void quit() {
        driver.quit();
    }
}



