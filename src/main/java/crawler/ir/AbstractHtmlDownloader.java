package crawler.ir;

import org.apache.log4j.Logger;

import java.util.HashSet;
import java.util.Set;

/**************************************************************
 * Abstract class for HtmlDownloader. It implements work with
 * failed links.
 *
 * @version 1.0
 * @author Jiří Plaček
 **************************************************************/
public abstract class AbstractHtmlDownloader implements IHtmlDownloader {

    /** Logger */
    static final Logger log = Logger.getLogger(AbstractHtmlDownloader.class);
    /** Set of failed links */
    Set<String> failedLinks = new HashSet<>();

    /**************************************************************
     * Returns failed links.
     *
     * @return Failed links
     **************************************************************/
    public Set<String> getFailedLinks() {
        return failedLinks;
    }

    /**************************************************************
     * Empty failed set.
     **************************************************************/
    public void emptyFailedLinks() {
        failedLinks = new HashSet<>();
    }
}
