package crawler.ir;

import java.util.List;
import java.util.Map;
import java.util.Set;

/**************************************************************
 * Interface for HtmlDownloader.
 *
 * @version 1.0
 * @author Jiří Plaček
 **************************************************************/
public interface IHtmlDownloader {

    /**************************************************************
     * Returns failed links.
     *
     * @return Failed links
     **************************************************************/
    Set<String> getFailedLinks();

    /**************************************************************
     * Empty failed set.
     **************************************************************/
    void emptyFailedLinks();


    /**************************************************************
     * Download given url page and extracts xpath expression.
     *
     * @param url      Page url
     * @param xpathMap Pairs of description and xpath expression
     * @return Pairs of descriptions and extracted values
     **************************************************************/
    Map<String, List<String>> processUrl(String url, Map<String, String> xpathMap);

    /**************************************************************
     * Download given url page and extracts xpath expression.
     *
     * @param url      Page url
     * @param xPath    Xpath expression
     * @return List of extracted values
     **************************************************************/
    List<String> getLinks(String url, String xPath);

    /**************************************************************
     * Finds maximal page number and return it.
     *
     * @param url      Page url
     * @param xPath    Xpath expression
     * @return Maximal page
     **************************************************************/
    int getMaximalPage(String url, String xPath);

    /**************************************************************
     * Quit driver (browser).
     **************************************************************/
    void quit();
}
