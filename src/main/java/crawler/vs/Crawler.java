package crawler.vs;

import crawler.ir.AbstractHtmlDownloader;
import crawler.ir.HtmlDownloaderSelenium;
import crawler.utils.Utils;
import cz.zcu.kiv.nlp.ir.trec.data.Document;
import org.apache.log4j.Logger;
import org.jsoup.Jsoup;
import utils.Record;

import java.io.File;
import java.util.*;

import static utils.Consts.*;

/**************************************************************
 * Main class of project.
 *
 * @version 1.0
 * @author Jiří Plaček
 **************************************************************/
public class Crawler {
    /** Xpath expression to gets wanted data */
    private final Map<String, String> xpathMap = new HashMap<>();

    /** Logger of Crawler class */
    private static final Logger log = Logger.getLogger(Crawler.class);

    /** List od downloaded data */
    private List<Document> downloadedData;

    /**************************************************************
     * Crawler class. It can download 30kmh forum.
     *
     * @version 1.0
     * @author Jiří Plaček
     **************************************************************/
    public Crawler() {
        downloadedData = new ArrayList<>();
    }


    /**************************************************************
     * Start downloading website.
     *
     * @param intoFile Save result into file
     **************************************************************/
    public void startDownloading(boolean intoFile){
        File outputDir = new File(STORAGE);

        if (!outputDir.exists()) {
            boolean mkdirs = outputDir.mkdirs();
            if (mkdirs) {
                log.info("Output directory created: " + outputDir);
            } else {
                log.error("Output directory can't be created! Please either create it or change the STORAGE parameter.\nOutput directory: " + outputDir);
            }
        }
        System.out.println("Zahajuji získávání odkazů.");

        //initialize donwloader
        AbstractHtmlDownloader downloader = new HtmlDownloaderSelenium();

        List<String> boards = new ArrayList<>(downloadBoardsLinks(downloader));

        //load boards links and download topics in every one
        List<String> topics = new ArrayList<>(downloadTopicsLinks(boards, downloader));

        //download posts
        downloadPosts(topics, downloader, intoFile);
    }

    /**************************************************************
     * Download links of forum boards.
     *
     * @param downloader    Downloader
     *
     * @return List of links of forum boards
     **************************************************************/
    private List<String > downloadBoardsLinks(AbstractHtmlDownloader downloader) {
        List<String> boards;
        String xPath = "//td[@class='windowbg2 info']/h4/a/@href";
        boards = downloader.getLinks(SITE, xPath);

        return boards;
    }

    /**************************************************************
     * Download links of board topics.
     *
     * @param boards        Boards links
     * @param downloader    Downloader
     *
     * @return List of links of boards topics
     **************************************************************/
    private List<String > downloadTopicsLinks(List<String> boards, AbstractHtmlDownloader downloader) {
        List<String> topics = new ArrayList<>();
        String cssMaxPage = "#modbuttons_bottom>div>a.navPages";
        String xPathTopic = "//td[contains(@class, 'subject')]//span/a/@href";

        //go through boards
        for(String board : boards) {
            //remove the last part of address (need to add it manually later)
            board = board.substring(0, board.lastIndexOf("."));

            //go through next pages
            int maxPage = (downloader.getMaximalPage(board, cssMaxPage) * BOARD_PAGING);
            for (int i = 0; i < maxPage; i += BOARD_PAGING ) {
                String link = board + "." + i;
                topics.addAll(downloader.getLinks(link, xPathTopic));
            }
        }

        return topics;
    }

    /**************************************************************
     * Download posts in topics.
     *
     * @param topics        Topics links
     * @param downloader    Downloader
     *
     * @return List of records
     **************************************************************/
    private List<Document> downloadPosts(List<String> topics, AbstractHtmlDownloader downloader, boolean intoFile) {
        System.out.println("Zahajuji stahování.");

        String cssMaxPage = "a.navPages";

        xpathMap.put(CONTENT_KEY, CONTENT_XPATH);
        xpathMap.put(TITLE_KEY, TITLE_XPATH);
        xpathMap.put(USER_KEY, USER_XPATH);

        //go through boards
        long id = 0;
        for(String topicLink : topics) {
            //remove the last part of address (need to add it manually later)
            topicLink = topicLink.substring(0, topicLink.lastIndexOf("."));

            //go through next pages
            int maxPage = (downloader.getMaximalPage(topicLink, cssMaxPage) * TOPIC_PAGING);
            for (int i = 0; i < maxPage; i += TOPIC_PAGING) {
                String link = topicLink + "." + i;

                //Proccess url
                Map<String, List<String>> result = downloader.processUrl(link, xpathMap);
                int cMax = result.get(CONTENT_KEY).size();

                for (int i2 = 0; i2 < cMax; i2++) {
                    //remove html tags
                    String content = Jsoup.parse(result.get(CONTENT_KEY).get(i2)).text();
                    String title = Jsoup.parse(result.get(TITLE_KEY).get(0)).text();
                    String user = Jsoup.parse(result.get(USER_KEY).get(i2)).text();

                    downloadedData.add(new Record(Long.toString(id), title, content, user));
                    id += 1;
                }
            }
        }

        //save into file if it is wanted
        String date = Utils.SDF.format(System.currentTimeMillis());
        if(intoFile) {
            Document.save(STORAGE + date + DEFAULT_FILE_EXTENSION, downloadedData);
            log.info("Records saved into file: " + STORAGE + date + DEFAULT_FILE_EXTENSION);
        }

        System.out.println("Stahování dokončeno.");
        System.out.println("Data uložena do souboru: " + STORAGE + date + DEFAULT_FILE_EXTENSION);

        return downloadedData;
    }

    /**************************************************************
     * Load already downloaded data.
     *
     * @param path Path to file. If string is empty, it will take
     *             from default path (./storage/data.ser)
     **************************************************************/
    public void loadData(String path) {
        if(path.isEmpty()) {
            path = STORAGE + DEFAULT_FILE_NAME + DEFAULT_FILE_EXTENSION;
        }
        downloadedData = Document.load(path);
    }

    /**************************************************************
     * Set downloaded data.
     *
     * @param downloadedData Downloaded data
     **************************************************************/
    private void setDownloadedData(List<Document> downloadedData) {
        this.downloadedData = downloadedData;
    }

    /**************************************************************
     * Return donwloaded data.
     *
     * @return Downloaded data
     **************************************************************/
    public List<Document> getDownloadedData() {
        return downloadedData;
    }
}
