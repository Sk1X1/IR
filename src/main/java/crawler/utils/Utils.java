package crawler.utils;

import java.io.*;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;

/**************************************************************
 * Class with util methods.
 *
 * @version 1.0
 * @author Jiří Plaček
 **************************************************************/
public class Utils {
    public static final java.text.DateFormat SDF = new SimpleDateFormat("yyyy-MM-dd_HH_mm_SS");
}
