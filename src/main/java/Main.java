import crawler.vs.Crawler;
import cz.zcu.kiv.nlp.ir.trec.SerializedDataHelper;
import cz.zcu.kiv.nlp.ir.trec.TestTrecEval;
import cz.zcu.kiv.nlp.ir.trec.data.Document;
import cz.zcu.kiv.nlp.ir.trec.data.IResult;
import indexer.Index;
import indexer.Indexer;
import indexer.utils.Posting;
import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import preprocess.vs.Preprocessing;
import search.ir.Searcher;
import search.vs.Search;

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import java.util.function.Function;
import java.util.stream.Collectors;

import static utils.Consts.*;


public class Main {

    /** Logger of Main class */
    private static final Logger log = Logger.getLogger(Main.class);

    public static void main(String[] args) {
        BasicConfigurator.configure();
        Logger.getRootLogger().setLevel(Level.OFF);

        if(args.length < 1) {
            printText();
            return;
        }

        switch (args[0]) {
            case DOWNLOAD_PAR:
                download();
                break;
            /*case PREPROCESS_PAR:
                preprocess(true);
                break;*/
            case INDEX_PAR:
                if(args.length < 2 ) {
                    System.out.println("Zadejte prosím soubor s daty, které chcete zaindexovat.");
                    return;
                }
                index(args[1]);
                break;
            case SEARCH_PAR:
                /*if(args.length < 2 ) {
                    //System.out.println("Zadejte prosím text, který chcete hledat.");
                    return;
                }*/
                search();
                break;
            case EVAL_PAR:
                eval(args);
                break;
             default:
                 printText();
                 break;
        }
    }

    /**
     * Print help text.
     */
    private static void printText() {
        System.out.println("Spusťte prosím program s jedním z následujících parametrů:");
        System.out.println("    -d  Stáhne data do souboru");
        //System.out.println("    -p  Provede preprocessing ze souboru \"data.ser\" a uloží ho do souboru \"preprocessData\"");
        System.out.println("    -i  soubor.ser  Načte data ze zadaného souboru a vytvoří nad nimi index, který uloží");
        System.out.println("        do souboru \"index\"");
        System.out.println("    -s  Zahájí vyhledávání nad existujícím indexem (případně ho dovytvoří)");
        System.out.println("Všechny soubory jsou načítány nebo ukládány z podadresáře ./storage/");
    }

    private static void eval(String[] args){
        TestTrecEval testTrecEval = new TestTrecEval();
        try {
            TestTrecEval.main(args);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Downloads data from 30kmh.
     */
    private static void download() {
        Crawler crawler = new Crawler();
        crawler.startDownloading(true);
    }

    /**
     * Preprocess data. Load data from data.ser and save preprocessed data
     * into preprocesData.
     */
    private static List<Document> preprocess(boolean intoFile) {
        Preprocessing preprocessing = new Preprocessing();
        preprocessing.preprocessData(intoFile);
        return preprocessing.getData();
    }

    /**
     * Preprocess data. Load data from inserted path and save preprocessed data
     * into preprocesData.
     */
    private static List<Document> preprocess(String path, boolean intoFile) {
        Preprocessing preprocessing = new Preprocessing(path);
        preprocessing.preprocessData(intoFile);
        return preprocessing.getData();
    }

    /**
     * Preprocess and index data into file index.txt.
     */
    private static void index(String path) {
        Preprocessing preprocessing = new Preprocessing(path);
        List<Document> data = preprocessing.getData();
        Indexer indexer = new Index();
        indexer.index(data, true);
    }

    /**
     * Index and search data.
     */
    private static void search() {
        //prepare variables
        Map<String, Document> documentMap = null;
        Map<String, Map<String, Posting>> invertedIndex = null;

        Indexer index = new Index();
        File indexFile = new File(STORAGE + INDEX_FILE_NAME);
        File documentMapFile = new File(STORAGE + DOCUMENT_MAP_FILE_NAME);

        //if files exist, load them, otherwise create new
        if(indexFile.exists() && documentMapFile.exists()) {
            FileInputStream fos = null;
            try {
                fos = new FileInputStream(indexFile);
                ObjectInputStream stream = new ObjectInputStream(fos);

                System.out.println("Začínám načítat index.");
                invertedIndex = (Map<String, Map<String, Posting>>) stream.readObject();
                stream.close();

                //load document map
                fos = new FileInputStream(documentMapFile);
                stream = new ObjectInputStream(fos);
                documentMap = (Map<String, Document>) stream.readObject();
                //documentMap = documents.stream().collect(Collectors.toMap(Document::getId, Function.identity()));
                stream.close();

                System.out.println("Index úspěšně načten.");
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            }

            if (invertedIndex == null || documentMap == null) {
                System.out.println("Soubor nebyl korektně načten.");
                return;
            }
        }
        else {
            //load documents
            File serializedData = new File(STORAGE + DEFAULT_FILE_NAME + DEFAULT_FILE_EXTENSION);
            List<Document> documents = new ArrayList<>();
            try {
                if (serializedData.exists()) {
                    documents = SerializedDataHelper.loadDocument(serializedData);
                } else {
                    log.error("Cannot find " + serializedData);
                    System.out.println("Nepodařilo se nalézt soubor s daty (./storage/data.ser).");
                    return;
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

            //create new index
            index.index(documents, true);
            invertedIndex = index.getInvertedIndex();
            documentMap = index.getDocumentMap();
        }


        Scanner sc =  new Scanner(System.in);
        String query = "e";
        while (!query.isEmpty()) {
            System.out.println("Zadejte prosím hledaný výraz:");
            query = sc.nextLine();

            if(query.isEmpty()) {
                return;
            }

            System.out.println("\nNalezené výsledky:");
            //Map<String, Document> documentMap = indexer.getDocumentMap();
            //Searcher searcher = new Search(indexer.getInvertedIndex(), indexer.getDocumentMap());
            Searcher searcher = new Search(invertedIndex, documentMap);
            //search for TOP 15
            for (IResult re : searcher.search(query, 15)) {
                Document doc = documentMap.get(re.getDocumentID());
                System.out.println(doc.getTitle());
                System.out.println(doc.getText());
                System.out.println("===================================================================");
            }
            System.out.println("===================================================================\n\n");
        }



    }
}