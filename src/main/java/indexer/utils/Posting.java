package indexer.utils;

import java.io.Serializable;

/**************************************************************
 * Class representing information about term in document.
 *
 * @version 1.0
 * @author Jiří Plaček
 **************************************************************/
public class Posting implements Serializable {

    /** Document id */
    private String documentId;
    /** Term frequency */
    private int termFrequency;
    /** Tf-idf */
    private double tf_idf = 0;

    /**************************************************************
     * Default constructor.
     *
     * @param documentId Document id
     * @param termFrequency Term frequency
     **************************************************************/
    public Posting(String documentId, int termFrequency) {
        this.documentId = documentId;
        this.termFrequency = termFrequency;
    }

    /**************************************************************
     * Return document id.
     *
     * @return Document id
     **************************************************************/
    public String getDocumentId() {
        return documentId;
    }

    /**************************************************************
     * Set document id.
     *
     * @param documentId Document id
     **************************************************************/
    public void setDocumentId(String documentId) {
        this.documentId = documentId;
    }

    /**************************************************************
     * Return term frequency.
     *
     * @return Term frequency
     **************************************************************/
    public int getTermFrequency() {
        return termFrequency;
    }

    /**************************************************************
     * Set term frequency
     *
     * @param termFrequency Term frequency
     **************************************************************/
    public void setTermFrequency(int termFrequency) {
        this.termFrequency = termFrequency;
    }

    /**************************************************************
     * Return tf-idf.
     *
     * @return tf-idf
     **************************************************************/
    public double getTf_idf() {
        return tf_idf;
    }

    /**************************************************************
     * Set tf-idf
     *
     * @param tf_idf Tf-idf
     **************************************************************/
    public void setTf_idf(double tf_idf) {
        this.tf_idf = tf_idf;
    }
}
