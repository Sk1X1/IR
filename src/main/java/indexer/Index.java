package indexer;

import cz.zcu.kiv.nlp.ir.trec.data.Document;
import indexer.utils.Posting;
import org.apache.log4j.Logger;
import preprocess.vs.Preprocessing;
import utils.Record;

import java.io.*;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static utils.Consts.DOCUMENT_MAP_FILE_NAME;
import static utils.Consts.INDEX_FILE_NAME;
import static utils.Consts.STORAGE;

/**************************************************************
 * Indexing class.
 *
 * @version 1.0
 * @author Jiří Plaček
 **************************************************************/
public class Index implements Indexer {

    /** Logger of Preprocessing class */
    private static final Logger log = Logger.getLogger(Index.class);

    /** Document map (document id : document) */
    private Map<String, Document> documentMap;
    /** Inverted index (term : document id) */
    private Map<String, Map<String, Posting>> invertedIndex;
    /** Preprocessing class */
    private Preprocessing preprocessing;



    /**************************************************************
     * Default constructor.
     **************************************************************/
    public Index() {
        documentMap = new HashMap<>();
        invertedIndex = new HashMap<>();
        preprocessing = new Preprocessing(false);
    }

    /**
     * Index using lucene preprocess.
     *
     * @param documents List of records
     * @param saveIntoFile If result should be saved into file
     */
    @Override
    public void index(List<Document> documents, boolean saveIntoFile) {
        System.out.println("Zahahuji indexování.");
        for (Document record : documents) {
            documentMap.put(record.getId(), record);

            List<String> tokens = preprocessing.preprocessRecord(record, true);
            for(String token : tokens) {
                addTokenInIndex(record.getId(), token);
            }
        }

        //calculate tf
        for(Map<String, Posting> postingMap : invertedIndex.values()) {
            //double norm = 0.0;
            double idf = Math.log10(documents.size() / postingMap.size());

            //find norm
            for(Posting p : postingMap.values()){
                if(p.getTermFrequency() > 0) {
                    p.setTf_idf((1 + Math.log10(p.getTermFrequency())) * idf);
                    //norm += p.getTf_idf() * p.getTf_idf();
                }
            }

            /*norm = Math.sqrt(norm);
            //set normalized tf
            for(Posting p : postingMap.values()){
                if(p.getTermFrequency() > 0) {
                    p.setTf_idf(p.getTf_idf() / norm);
                }
            }*/

        }

        //save index into file
        if (saveIntoFile) {
            try {
                File fout = new File(STORAGE + INDEX_FILE_NAME);
                FileOutputStream fos = new FileOutputStream(fout);
                ObjectOutputStream stream = new ObjectOutputStream(fos);
                stream.writeObject(invertedIndex);
                stream.close();

                fout = new File(STORAGE + DOCUMENT_MAP_FILE_NAME);
                fos = new FileOutputStream(fout);
                stream = new ObjectOutputStream(fos);
                stream.writeObject(documentMap);
                stream.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        System.out.println("Indexování dokončeno.");
    }

    /**************************************************************
     * Index using custom preprocessing.
     *
     * @param documents List of records
     * @param intoFile Save result into file
     **************************************************************/
    @Override
    public void customIndex(List<Record> documents, boolean intoFile) {
        //set up timer to calculate duration
        long start = System.nanoTime();
        //set up preprocessing
        Preprocessing preprocessing = new Preprocessing(false);
        for(Record document : documents) {
            documentMap.put(document.getId(),document);

            //preprocess document (get tokens with frequencies)
            Map<String, Integer> tokens = preprocessing.preprocessRecord2(document);

            //process tokens from document
            for(Map.Entry<String, Integer> token : tokens.entrySet()) {
                //token is already inside index
                Map<String, Posting> postingMap;
                if ((postingMap = invertedIndex.get(token.getKey())) != null) {
                    //i dont have to check, if document is already listed because I go
                    //through documents and get tokens with frequency from preprocessing class
                    //no token twice in collection

                    //document not in list, add it
                    postingMap.put(document.getId(), new Posting(document.getId(), token.getValue()));
                }
                else {
                    //token not in index, add it with new posting list
                    postingMap = new HashMap<>();
                    postingMap.put(document.getId(), new Posting(document.getId(), token.getValue()));
                    invertedIndex.put(token.getKey(), postingMap);
                }
            }
        }

        //print duration
        long elapsedTime = System.nanoTime() - start;
        log.info("Duration: " + (elapsedTime / 1000000000) + " sec");

        //calculate tf
        for(Map<String, Posting> postingMap : invertedIndex.values()) {
            double idf = Math.log10(documents.size() / postingMap.size());
            for(Posting p : postingMap.values()){
                p.setTf_idf((1 + Math.log(p.getTermFrequency())) * idf);
            }
        }
        //save into file if it is wanted
        if(intoFile) {
            try {
                File fout = new File(STORAGE + INDEX_FILE_NAME);
                FileOutputStream fos = new FileOutputStream(fout);
                BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(fos));

                for (Map.Entry<String, Map<String, Posting>> m : invertedIndex.entrySet()) {

                    bw.write(m.getKey() + " - ");
                    for (Posting p : m.getValue().values()) {
                        bw.write("[" + p.getDocumentId() + ", " + p.getTermFrequency() + " - " + p.getTf_idf() + "]" + ", ");
                    }
                    bw.newLine();
                }

                bw.newLine();
                bw.close();
            }
            catch (IOException e){
                e.printStackTrace();
            }
        }
    }

    /**
     * Add token into inverted index
     * @param token Token to insert
     */
    private void addTokenInIndex(String docId, String token) {
        Map<String, Posting> postingMap;
        //find token in inverted index
        if ((postingMap = invertedIndex.get(token)) != null) {
            //token found, found current document
            Posting p;
            if((p = postingMap.get(docId)) != null) {
                //document already listed, just increase token frequency
                p.setTermFrequency(p.getTermFrequency() + 1);
            }
            else {
                //document not listed, add it
                postingMap.put(docId, new Posting(docId, 1));
            }
        }
        else {
            //token not in index, add it with new posting list
            postingMap = new HashMap<>();
            postingMap.put(docId, new Posting(docId, 1));
            invertedIndex.put(token, postingMap);
        }
    }

    public Map<String, Map<String, Posting>> getInvertedIndex() {
        return  this.invertedIndex;
    }

    public Map<String, Document> getDocumentMap() {
        return documentMap;
    }

}
