package indexer;

import cz.zcu.kiv.nlp.ir.trec.data.Document;
import indexer.utils.Posting;
import utils.Record;

import java.util.List;
import java.util.Map;

/**************************************************************
 * Indexer interface.
 *
 * @version 1.0
 * @author Jiří Plaček
 **************************************************************/
public interface Indexer {

    /**************************************************************
     * Index documents with lucene.
     *
     * @param documents List of records
     * @param saveIntoFile If result should be saved into file
     **************************************************************/
    void index(List<Document> documents, boolean saveIntoFile);

    /**************************************************************
     * Custom index.
     *
     * @param documents List of records
     * @param saveIntoFile If result should be saved into file
     **************************************************************/
    void customIndex(List<Record> documents, boolean saveIntoFile);

    /**************************************************************
     * Return inverted index.
     *
     * @return Inverted index
     **************************************************************/
    Map<String, Map<String, Posting>> getInvertedIndex();

    /**************************************************************
     * Return document map.
     *
     * @return Document map
     **************************************************************/
    Map<String, Document> getDocumentMap();
}
