package cz.zcu.kiv.nlp.ir.trec.data;

import java.io.*;
import java.util.Date;
import java.util.List;

/**
 * Created by Tigi on 8.1.2015.
 */
public interface Document {

    String getText();

    String getId();

    String getTitle();

    Date getDate();

    /**************************************************************
     * Save list of records into file.
     *
     * @param path  Path to file
     * @param list  List of records
     **************************************************************/
    static void save(String path, List<Document> list) {
        try {
            FileOutputStream fileOut = new FileOutputStream(path);
            ObjectOutputStream out = new ObjectOutputStream(fileOut);

            out.writeObject(list);

            out.close();
            fileOut.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**************************************************************
     * Load list of records from file.
     *
     * @param path Path to file with records
     * @return Lost of documents
     **************************************************************/
    static List<Document> load(String path) {
        File records = new File(path);
        final Object object;
        try {
            final ObjectInputStream objectInputStream = new ObjectInputStream(new FileInputStream(records));
            object = objectInputStream.readObject();
            objectInputStream.close();

            return (List<Document>) object;
        } catch (Exception ex) {
            throw new RuntimeException(ex);
        }
    }
}
