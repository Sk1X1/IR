package cz.zcu.kiv.nlp.ir.trec;

import cz.zcu.kiv.nlp.ir.trec.data.*;
import indexer.Index;
import indexer.Indexer;
import indexer.utils.Posting;
import org.apache.log4j.*;
import preprocess.vs.Preprocessing;
import search.ir.Searcher;
import search.vs.Search;

import java.io.*;
import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

import static utils.Consts.INDEX_FILE_NAME;
import static utils.Consts.STORAGE;


/**
 * @author tigi
 */

public class TestTrecEval {

    static Logger log = Logger.getLogger(TestTrecEval.class);
    static final String OUTPUT_DIR = "./TREC";

    protected static void configureLogger() {
        BasicConfigurator.resetConfiguration();
        BasicConfigurator.configure();

        File results = new File(OUTPUT_DIR);
        if (!results.exists()) {
            results.mkdir();
        }

        try {
            Appender appender = new WriterAppender(new PatternLayout(), new FileOutputStream(new File(OUTPUT_DIR + "/" + SerializedDataHelper.SDF.format(System.currentTimeMillis()) + " - " + ".log"), false));
            BasicConfigurator.configure(appender);
        } catch (IOException e) {
            e.printStackTrace();
        }

        Logger.getRootLogger().setLevel(Level.INFO);
    }

    public static void main(String args[]) throws IOException {
        configureLogger();

//        todo constructor

        List<Topic> topics = SerializedDataHelper.loadTopic(new File(OUTPUT_DIR + "/topicData.bin"));

        File serializedData = new File(OUTPUT_DIR + "/czechData.bin");

        List<Document> documents = new ArrayList<>();
        log.info("load");
        try {
            if (serializedData.exists()) {
                documents = SerializedDataHelper.loadDocument(serializedData);
            } else {
                log.error("Cannot find " + serializedData);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        log.info("Documents: " + documents.size());

        List<String> lines = new ArrayList<String>();


        //Map<String, Document> documentMap = null;
        Map<String, Map<String, Posting>> invertedIndex = null;
        Map<String, Document> documentMap = null;

        Indexer index = new Index();
        File fout = new File(STORAGE + INDEX_FILE_NAME);
        //if exist, load it, otherwise create new index
        if(fout.exists()) {
            FileInputStream fos = null;
            try {
                fos = new FileInputStream(fout);
                ObjectInputStream stream = new ObjectInputStream(fos);

                System.out.println("Začínám načítat index.");

                invertedIndex = (Map<String, Map<String, Posting>>) stream.readObject();

                stream.close();

                //load document map
                documentMap = documents.stream().collect(Collectors.toMap(Document::getId, Function.identity()));

                System.out.println("Index úspěšně načten.");
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            }

            if (invertedIndex == null || documentMap == null) {
                System.out.println("Soubor nebyl korektně načten.");
                return;
            }
        }
        else {
            index.index(documents, true);
            invertedIndex = index.getInvertedIndex();
            documentMap = index.getDocumentMap();
        }

        final File outputFile = new File(OUTPUT_DIR + "/results " + SerializedDataHelper.SDF.format(System.currentTimeMillis()) + ".txt");

        Scanner sc =  new Scanner(System.in);
        String query = "e";
        while (!query.isEmpty()) {

            System.out.println("Pro spuštění vyhodnocení zadej neprázdný řetězec:");
            query = sc.nextLine();

            if(query.isEmpty()) {
                return;
            }

            //Searcher searcher = new Search(index.getInvertedIndex(), index.getDocumentMap());
            Searcher searcher = new Search(invertedIndex, documentMap);

            for (Topic t : topics) {
                List<IResult> resultHits = searcher.search(t.getTitle() + " " + t.getDescription(), 1000);

                Comparator<IResult> cmp = new Comparator<IResult>() {
                    public int compare(IResult o1, IResult o2) {
                        if (o1.getScore() > o2.getScore()) return -1;
                        if (o1.getScore() == o2.getScore()) return 0;
                        return 1;
                    }
                };

                //Collections.sort(resultHits, cmp);
                for (IResult r : resultHits) {
                    final String line = r.toString(t.getId());
                    lines.add(line);
                }
                if (resultHits.size() == 0) {
                    lines.add(t.getId() + " Q0 " + "abc" + " " + "99" + " " + 0.0 + " runindex1");
                }
            }
            IOUtils.saveFile(outputFile, lines);
        }

        //try to run evaluation
        try {
            runTrecEval(outputFile.toString());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private static String runTrecEval(String predictedFile) throws IOException {

        String commandLine = "./trec_eval.8.1/./trec_eval" +
                " ./trec_eval.8.1/czech" +
                " \"" + predictedFile + "\"";

        System.out.println(commandLine);
        Process process = Runtime.getRuntime().exec(commandLine);

        BufferedReader stdout = new BufferedReader(new InputStreamReader(process.getInputStream()));
        BufferedReader stderr = new BufferedReader(new InputStreamReader(process.getErrorStream()));

        String trecEvalOutput;
        StringBuilder output = new StringBuilder("TREC EVAL output:\n");
        for (String line; (line = stdout.readLine()) != null; ) output.append(line).append("\n");
        trecEvalOutput = output.toString();
        System.out.println(trecEvalOutput);

        int exitStatus = 0;
        try {
            exitStatus = process.waitFor();
        } catch (InterruptedException ie) {
            ie.printStackTrace();
        }
        System.out.println(exitStatus);

        stdout.close();
        stderr.close();

        return trecEvalOutput;
    }
}
