package cz.zcu.kiv.nlp.ir.trec;

import cz.zcu.kiv.nlp.ir.trec.data.IResult;

import java.util.List;

/**
 * Created by Tigi on 6.1.2015.
 */
public interface Searcher {
    List<IResult> search(String query);
    List<IResult> search(String query, int top);
}
